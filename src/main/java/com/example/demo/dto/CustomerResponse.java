package com.example.demo.dto;

import java.time.LocalDateTime;

public class CustomerResponse {
    private int customerId;
    private String customerName;
    private LocalDateTime registeredDate;

    public CustomerResponse(int customerId, String customerName, LocalDateTime registeredDate) {
        this.customerId = customerId;
        this.customerName = customerName;
        this.registeredDate = registeredDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public LocalDateTime getRegisteredDate() {
        return registeredDate;
    }
}
