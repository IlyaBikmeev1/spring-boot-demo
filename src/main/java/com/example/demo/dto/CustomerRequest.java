package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerRequest {
    @JsonProperty("name")
    private String name;

    public CustomerRequest() {
    }

    public CustomerRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
