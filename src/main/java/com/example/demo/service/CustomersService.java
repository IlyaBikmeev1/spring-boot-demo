package com.example.demo.service;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomersService {

    private final List<CustomerResponse> customersState = new ArrayList<>(List.of(
        new CustomerResponse(
            1, "Ilya Bikmeev", LocalDateTime.now()
        ),
        new CustomerResponse(
            2, "Ivan Ivanov", LocalDateTime.now()
        ),
        new CustomerResponse(
            3, "Kirill Borovets", LocalDateTime.now()
        ),
        new CustomerResponse(
            4, "Petr Petrov", LocalDateTime.now()
        ),
        new CustomerResponse(
            5, "Rafael Nadal", LocalDateTime.now()
        ))
    );

    public List<CustomerResponse> findAll() {
        return customersState;
    }

    public CustomerResponse findById(int id) {
        return customersState.stream()
            .filter(c -> c.getCustomerId() == id)
            .findFirst().orElseThrow();
    }

    public CustomerResponse createCustomer(CustomerRequest request) {
        CustomerResponse result = new CustomerResponse(customersState.size() + 1, request.getName(), LocalDateTime.now());
        customersState.add(result);
        return result;
    }
}
