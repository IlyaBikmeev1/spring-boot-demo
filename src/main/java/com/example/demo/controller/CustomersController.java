package com.example.demo.controller;

import com.example.demo.dto.CustomerRequest;
import com.example.demo.dto.CustomerResponse;
import com.example.demo.service.CustomersService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomersController {
    private final CustomersService customersService;

    public CustomersController(CustomersService customersService) {
        this.customersService = customersService;
    }

    @GetMapping("/customers")
    public List<CustomerResponse> getAllCustomers() {
        return customersService.findAll();
    }

    @GetMapping("/customer/{id}")
    public CustomerResponse getById(@PathVariable int id) {
        return customersService.findById(id);
    }

    @PostMapping("/customers")
    public CustomerResponse createNewCustomer(@RequestBody CustomerRequest request) {
        return customersService.createCustomer(request);
    }
}
